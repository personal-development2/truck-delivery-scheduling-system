var deliveryService = require('../service/DeliveryService')

exports.getDeliveries = (req, res) => {
    return deliveryService.getDeliveries(req, res);
};

exports.postNewDelivery = (req, res) => {
   return deliveryService.postNewDelivery(req, res); 
};

exports.updateStatus = (req, res) => {
    return deliveryService.updateStatus(req, res);
};