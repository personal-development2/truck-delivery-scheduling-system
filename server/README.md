# server
Provide APIs for web app to call
1. `GET /v1/api/deliveries` - Retrieve all deliveries information
2. `GET /v1/api/trucks` - Retrieve all trucks information
3. `GET /v1/api/truck?truckId` - Retrieve specific truck information by providing the `truckId`
4. `POST /v1/api/delivery/new` - Create new delivery. Success only when delivery datetime is available
5. `PATCH /v1/api/delivery/{deliveryId}/status?status` - Update delivery status of specific delivery

## Postman Collection
Import the postman collection located on root folder to your postman
`truck-delivery-scheduling-system.postman_collection.json` 


## MongoDB
Install mongodb and start on local machine

**start MongoDB**
```
brew services start mongodb-community@4.4
```
**stop MongoDB**
```
brew services stop mongodb-community@4.4
```

Open server.js and change `localhost/mydb` to the one you created on mongodb
```
mongoose.connect('mongodb://localhost/mydb', {
```

## Project setup
```
npm install
```

## Start
```
node server.js
```