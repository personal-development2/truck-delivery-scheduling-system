import axios from "axios";

const url = "http://localhost:8080/v1/api";
const headers = {
  'Access-Control-Allow-Origin': '*'
}
export function getTrucks() {
  return axios.get(url + "/trucks", {headers}).then(response => response.data);
}
export function getDeliveries() {
  return axios.get(url + "/deliveries", {headers}).then(response => response.data);
}
export function postDeliveries(requestBody) {
  return axios.post(url + "/delivery/new", requestBody, {headers}).then(response => response.data);
}
export function patchDeliveries(requestBody) {
  return axios.patch(url + "/delivery/{deliveryId}/status", requestBody, {headers}).then(response => response.data);
}
