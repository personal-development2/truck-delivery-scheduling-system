import React, {useState} from 'react';
import {Button, Modal} from "antd";

function UpdateDeliveryModal(props) {
  const [visible, setVisible] = React.useState(false);
  const [confirmLoading, setConfirmLoading] = React.useState(false);

  const showModal = () => {
    setVisible(true);
  };

  const handleOk = () => {
    setConfirmLoading(true);
    setTimeout(() => {
      setVisible(false);
      setConfirmLoading(false);
    }, 2000);
  };

  const handleCancel = () => {
    console.log('Clicked cancel button');
    setVisible(false);
  };

  return (
    <>
      <Button type="primary" onClick={showModal}>
        Update Status
      </Button>
      <Modal
        title="Update Status"
        visible={visible}
        onOk={handleOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
      >       
      <p>Do you want to update delivery status?</p>
      </Modal>
      
    </>
  );
}
export default UpdateDeliveryModal;
